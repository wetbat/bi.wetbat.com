## Instalation && Configuration

To start up the BI, just execute the following command and then access http://localhost:3002. 

```bash
docker compose up -d --build
```

To access the BI in production, [click here](http://195.35.37.192:3002)